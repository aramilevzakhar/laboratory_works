import pygame as pg
import sys
import random
#import person
#iimport bomb


pg.init()
width = 800
height = 500
window = pg.display.set_mode((width, height)) # general window
pg.draw.rect(window, (16, 16, 255), (0, 0, width, height//2)) # sky
pg.draw.rect(window, (16, 255, 16), (0, height//2, width, height//2)) # green
font = pg.font.SysFont('Bront', 20) 


pg.display.update()


# название картинок
image_names_r = [
	'image/g1.png',
	'image/g2.png',
	'image/g3.png',
	'image/g4.png',
	'image/g5.png',
	'image/g6.png']
image_names_l = [
	'image/g1r.png',
	'image/g2r.png',
	'image/g3r.png',
	'image/g4r.png',
	'image/g5r.png',
	'image/g6r.png']

personl = []
personr = []

# спрайты персонажа left right
for e in zip(image_names_r, image_names_l):
	personr.append(pg.image.load(e[0]))
	personl.append(pg.image.load(e[1]))
# изображение статичного персонажа
person_stay = pg.image.load('image/g_stay.png')
# изображение бомбочки и еды
bomb = pg.image.load('image/bomb.png')
eyat = pg.image.load('image/xxx.png')


points = 0  		# очки игровые
x = 390 			# позиция персонажа
y = 240 			# 
start = 0			# время когда клавиша нажата
way = 5 			# один шаг персонажа
count = 0			# переменная для переключения картинок персонажа



bomb_images = [bomb, eyat] # картинки падающих предметов
bomb_count = 25  # количество бомбочек и мячиков
bomb_list = []  # список бобмочек - как спавн препятствий
spavn_y = 0  # начальный отступ между бомбочками
print(random.random())
for _ in range(bomb_count):
	index = random.randint(0, 1)
	img = bomb_images[index]  # случайный рисунок бомбочки
	xx = random.randint(40, width-40)  # случайный отступ по X
	spavn_y += int(1.0 * random.randint(0, height))
	yy = 25 - spavn_y  # отступ по Y с накоплением от предыдущей бомбочки
	v = 1  # случайная скорость падения
	bomb = [img, xx, yy, v, True, index]  # для каждой бомбочки свои параметры
	bomb_list.append(bomb)  # добавляем бомбу в список

	clock = pg.time.Clock()  # объект для отсчёта времени
	time_fall = 0  # текущее время задержки падения всех бомбочек
	int_fall = 10  # интервал до следующего сдвига mlsek
backup = bomb_list
b = True
count1 = 0
while True:
	for e in pg.event.get():
		if e.type == pg.KEYUP and e.key == pg.K_RIGHT:
			b = True
		if e.type == pg.KEYDOWN and e.key == pg.K_RIGHT and x <= 780:
			b = False
			start = pg.time.get_ticks()
			way = 5
			x += way
			count = 0
		if e.type == pg.KEYUP and e.key == pg.K_LEFT:
			b = True
		if e.type == pg.KEYDOWN and e.key == pg.K_LEFT and x >= 0:
			b = False
			start = pg.time.get_ticks()
			way = -5
			x += way
			count = 0
		if e.type == pg.QUIT:
			pg.quit()
		if e.type == pg.KEYUP and e.key == pg.K_ESCAPE:
			sys.exit()
		#print(e)
	stop = pg.time.get_ticks()
	# если клавиша удерживается то персонаж двигается 
	if b == False and stop - start > 200 and (x >= 0 and x <= 780) and stop % 10 == 0:
		x += way
		count += 1
		count = count % 6
	#каждые 2 секунды с неба падает еда
	#if pg.time.get_ticks() % 2000 == 0:
	#while yy < 240:
	
	# sky and green
	pg.draw.rect(window, (16, 16, 255), (0, 0, width, height//2))
	pg.draw.rect(window, (16, 255, 16), (0, height//2, width, height//2))



	# draw mark
	window.blit(font.render('points: '+str(points),True,(0,0,0)),(5,5))

	# бомбочка
	clock.tick()  # в каждой итерации фиксируем время
	time_fall += clock.get_rawtime()  # накапливаем время паузы
	if time_fall >= int_fall:  # если превысили интервал паузы
		time_fall = 0  # сбрасываем для следующего цикла паузы
		for i in range(bomb_count):  # для всех бомб из списка
			if bomb_list[i][4]:  # если бомба ещё не достигла дна
				bomb_list[i][2] += bomb_list[i][3]  # смещаем её на шаг
	for i in range(bomb_count):  # для всех бомб из списка
		if bomb_list[i][4]:  # если бомба ещё не достигла дна
			# условия при которых очки будут прибавляться либо анулированны
			if (x - 20 <= bomb_list[i][1] and x  >= bomb_list[i][1]) and (bomb_list[i][2] == 240):
				if bomb_list[i][5] == 1:
					points += 1
					bomb_list[i][4] = False
				else:
					points = 0
			window.blit(bomb_list[i][0], (bomb_list[i][1], bomb_list[i][2]))  # рисуем бомбу
			if bomb_list[i][2] > height - 250:  # если бомба достигла дна
				bomb_list[i][4] = False  # отключаем бомбу
	
	# стоит на месте если ничего не проиходит иначе бежит вправо или влево
	if b == True:
		window.blit(person_stay, (x, y))
	else:
		if way > 0:
			window.blit(personr[count], (x, y))
		else:
			window.blit(personl[count], (x, y))
	#pg.draw.rect(window, (0, 0, 0), (x, y, 20, 20))

	# чтобы бомбы и мячики не когда не закачивались
	count1 += 1
	if count1 % 25 == 0:
		bomb_list = backup
		count1 = 0
	pg.display.update()
pg.quit()
