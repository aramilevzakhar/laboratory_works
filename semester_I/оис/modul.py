import random

def ran (n, d1, d2):
	arr = []
	for i in range (n):
		arr.append (random.randint (d1, d2))
		# print (i,':', arr[i])
	return arr

# сортировка выбором
def sort_the_choice (arr):
	for i in range (len (arr)):
		for j in range (len (arr)):
			if int (arr[i]) > int (arr[j]) and i < j:
				tmp = arr[i]
				arr[i] = arr[j]
				arr[j] = tmp
	return arr

# сортировка пузырьком
def sort_Bubble (arr):
	while True:
		boo = True
		for j in range (len (arr) - 1):
			if (arr[j] > arr[j + 1]):
				arr[j], arr[j + 1] = arr[j + 1], arr[j]
				boo = False
		if boo == True:
			break
	return arr
