class Student:
	def __init__(self, full_name, age, grade_point_average):
		self.full_name = full_name
		self.age = int(age)
		self.grade_point_average = float(grade_point_average)
	def Show_age(self):
		print(self.age, end = '')
	def Show_full_name(self):
		print(self.full_name, end = '')
	def Show_grade_point_average(self):
		print(self.grade_point_average, end = '')
	def Show_format_full_name (self):
		m = self.full_name.split (' ')
		return m[0] + ' ' + m[1][0] + '.' + m[2][0] + '.'
